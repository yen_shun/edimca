package com.javasampleapproach.springrest.postgresql.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

@Entity
@Table(name = "customer")
public class Customer {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column(name = "name")
	private String name;

	@Column(name = "description")
	private String description;	
	
	@Column(name = "priority")
	private String priority;		
	
	@Column(name = "age")
	private int age;
	
	@Column(name = "state")
	private String state;
	
	@Column(name = "technician")
	private String technician;	

	@Column(name = "active")
	private boolean active;	
	
	@Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created", nullable = false)
    private Date created;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated", nullable = false)
    private Date updated;

    @PrePersist
    protected void onCreate() {
    updated = created = new Date();
    }

    @PreUpdate
    protected void onUpdate() {
    updated = new Date();
    }
	
	public Customer() {
	}

	public Customer(String name, String description, String priority) {
		this.name = name;
		this.age = age;
		this.description = description;
		this.priority = priority;
		this.state = "pending";
		this.active = false;		
		this.created = new Date();
		this.updated = new Date();
		this.technician = "";
		//LocalDateTime now = LocalDateTime.now();

	}

	public long getId() {
		return id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return this.name;
	}
	
	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getCreated() {
		return this.created;
	}
	
	public void setUpdated(Date updated) {
		this.updated = updated;
	}

	public Date getUpdated() {
		return this.updated;
	}
	public void setPriority(String priority) {
		this.priority = priority;
	}

	public String getPriority() {
		return this.priority;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}

	public String getDescription() {
		return this.description;
	}
	
	public void setState(String state) {
		this.state = state;
	}

	public String getState() {
		return this.state;
	}
	
	public void setTechnician(String technician) {
		this.technician = technician;
	}

	public String getTechnician() {
		return this.technician;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public int getAge() {
		return this.age;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	@Override
	public String toString() {
		return "Customer [id=" + id + ", name=" + name +  ", description=" + description + ", state=" + state + ", priority=" + priority + ", age=" + age +  ", created=" + created + ", updated=" + updated + ", active=" + active + "]";
	}
}
