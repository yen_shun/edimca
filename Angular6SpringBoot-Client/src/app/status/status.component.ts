import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../customer.service';
import { Customer } from '../customer';

import { CustomersListComponent } from '../customers-list/customers-list.component';


@Component({
  selector: 'app-status',
  templateUrl: './status.component.html',
  styleUrls: ['./status.component.css']
})
export class StatusComponent implements OnInit {
  state: string;
  customers: Customer[];

  constructor(private dataService: CustomerService) { }

  ngOnInit() {
    this.state = "approval";
    this.dataService.getApprovalList(this.state)
      .subscribe(customers => this.customers = customers);
  }

  getResults() {
    this.dataService.getApprovalList(this.state)
      .subscribe(customers => this.customers = customers);
  }

}
