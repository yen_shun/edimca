export class Customer {
    id: number;
    name: string;
    age: number;
    description: string;
    priority: string;
    state: string;
    technician: string;
    active: boolean;
}
