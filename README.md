# Proyecto EDIMCA 

Este es un proyecto para comprobar la capacidad del programador para levantar ambientes, programar backend, instalar base de datos y usar angular como frontend.

## Comenzando 🚀

Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas.

Mira **Deployment** para conocer como desplegar el proyecto.


### Pre-requisitos 📋

Tener instalado el Node.js de preferencia la última versión (v10.15.3) puede descargar desde https://nodejs.org/es/ 
Comprobar la versión de node
```
node --version
```

* Java (JDK, JRE, y las variables locales seteados)
* PostgreSQL 11
* Maven como manejador de dependencias
* Apache Tomcat 8.5
* Spring Boot
* Spring Tool Suite
* Angular 6
* Git

### Instalación 🔧

* Descargar el proyecto de edimca 
* Tener apache Tomcat 8.5 o superior ejecutando y escuchando desde el puerto 8080

```
http://localhost:8080/
```

* Con el terminal de comando, entrar a la carpeta Angular6SpringBoot-Client y ejecutar ng serve. Mantener este terminal abierto para probar la parte Front-end de la aplicación

```
ng serve
```

Si muestra problemas o incovenientes, ejecutar npm install primero dentro de la carpeta Angular6SpringBoot-Client

```
npm install
```

Verificar que Angular corra en el puerto 4200. 

```
http://localhost:4200
```

Instalar PostgreSQL con el usuario "postgres" y la contraseña "1234". Al instalar correctamente al correr el siguiente comando dentro de la carpeta bin de PostgreSQL

```
psql --username="postgres" -W
```
se puede acceder al gestor de base de datos y ejecutar el siguiente comando
```
\l
```
Se mostrará las bases de datos que existe actualmente, si en la lista no se encuentra la base de datos llamada edimca, crear una nueva base de datos con ese nombre

```
create database edimca;
```
se puede ejecutar el comando \l para verificar nuevamente si se creó la base de datos, para usar dicha base de datos con el terminal se accede de la siguiente manera

```
 \c edimca;
```

## Arquitectura ⚙️

* La aplicación utiliza el controlador REST para las operaciones POST/GET/PUT/DELETE.
* El archivo donde se encuentra las operaciones POST/GET/PUT/DELETE se encuentra dentro del archivo CustomerController.java 
* La aplicacion hace conexion con la base de datos de tipo PostgreSQL llamado edimca y usa la tabla customer.
* La base de la aplicación se encuentra desarrollado en JAVA dentro de la clase Customer
* La configuración de la base de datos se encuentra dentro del proyecto MAVEN en el archivo application.properties
* Ver la arquitectura de Angular en el archivo AngularArchitecture.png dentro de la raíz del proyecto.


## Deployment 📦

Usar SpringTool Suite como  IDE

 * Ir a file->import
 * Maven
 * Existing Maven Project

Escoger la carpeta SpringRESTPostgreSQL dentro del proyecto de edimca
Select All y Finish

Para ejecutar el programa, hacer clic derecho al proyecto "spring-boot-rest-postgresql"
 * Run As -> Spring Boot App

Al correr el programa, está listo para poder realizar las llamadas POST/GET/PUT/DELETE y  en la consola debe visualizar un mensaje de "Get all Customers"

Visitar la ruta http://localhost:4200 para correr el aplicativo para el usuario final.

```
http://localhost:4200
```
Al visitar la página el usuario va a contar con 3 opciones

* Manager
* Employee
* Approval Page


1. Manager: en esta sección se encuentra las operaciones que puede realizar el usuario Manager. 

   Las operaciones son:

   1.1 Cambiar el estado del ticket
   	 Con el boton "Approval" el manager puede cambiar el estado a del ticket a "approval"

   1.2 Asignar al técnico correspondiente

     Existe 3 opciones de técnico (Pedro,Alejandro y Josselyn), al momento de hacer click en el nombre correspondiente, se asignará al técnico elegido.

2. Employee

   En esta sección el usuario employee puede generar un nuevo ticket.

3. Approval Page
	
	En esta sección muestra los tickets en estado "approval" y ordenado descendientemente de la fecha de modificación

## Construido con 🛠️


* [Spring Tools 4](https://spring.io/tools) - El framework web usado
* [Maven](https://maven.apache.org/) - Manejador de dependencias
* [Angular6](https://angular.io/guide/quickstart) - Herramienta Front-end


## Wiki 📖

Puedes encontrar mucho más de cómo utilizar este proyecto en nuestra [Wiki](https://gitlab.com/yen_shun/edimca)

## Versionado 📌

Se está usando gitlab como motor de versionamiento. Se puede encontrar en el siguiente enlace: https://gitlab.com/yen_shun/edimca.

## Autor ✒️


* **Yen Shun Wang** - *Trabajo y documentación* - [yen_shun](https://gitlab.com/yen_shun)

## Licencia 📄

Este proyecto está bajo la Licencia (Yen Shun)

## Expresiones de Gratitud 🎁

* Gracias al equipo de Aglomerados Cotopaxi por darme la oportunidad de desarrollar un proyecto de prueba 📢
