--
-- PostgreSQL database dump
--

-- Dumped from database version 11.3
-- Dumped by pg_dump version 11.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: customer; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.customer (
    id bigint NOT NULL,
    active boolean,
    age integer,
    name character varying(255),
    description character varying(255),
    priority character varying(255),
    created timestamp without time zone,
    updated timestamp without time zone,
    state character varying(255),
    technician character varying(255)
);


ALTER TABLE public.customer OWNER TO postgres;

--
-- Name: hibernate_sequence; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.hibernate_sequence OWNER TO postgres;

--
-- Data for Name: customer; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.customer (id, active, age, name, description, priority, created, updated, state, technician) FROM stdin;
19	f	0	yeb	\N	\N	2019-05-25 15:16:32.751	2019-05-25 17:03:57.5	approval	Josselyn
16	t	0	\N	test1	high	\N	2019-05-25 18:08:22.199	approval	Josselyn
18	f	10	test54	test54	low	2019-05-25 10:25:35.783	2019-05-25 18:08:23.431	approval	Josselyn
17	f	0	test2	test2	high	2019-05-24 23:46:45.387	2019-05-25 18:19:23.325	assigned	Josselyn
20	f	0	test ticket 1	this is other test ticket	low	2019-05-25 20:52:24.097	2019-05-25 20:53:28.99	approval	
\.


--
-- Name: hibernate_sequence; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.hibernate_sequence', 20, true);


--
-- Name: customer customer_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.customer
    ADD CONSTRAINT customer_pkey PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--

